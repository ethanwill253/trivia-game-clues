from fastapi import APIRouter, Response, status
from pydantic import BaseModel
import psycopg
from datetime import datetime

from .clues import Clue
from .categories import Message

# Using routers for organization
# See https://fastapi.tiangolo.com/tutorial/bigger-applications/
router = APIRouter()


class GameOut(BaseModel):
    id: int
    episode_id: int
    aired: str
    canon: bool
    total_amount_won: int


class CustomGame(BaseModel):
    id: int
    created_on: datetime
    clues: list[Clue]


@router.get(
    "/api/games/{game_id}",
    response_model=GameOut,
    responses={404: {"model": Message}},
)
def get_game(game_id: int, response: Response):
    with psycopg.connect() as conn:
        with conn.cursor() as cur:
            cur.execute(
                """
                SELECT g.id, g.episode_id, g.aired, g.canon
                    , SUM(clues.value) AS total_amount_won
                FROM games AS g
                LEFT OUTER JOIN clues ON (clues.game_id = g.episode_id)
                WHERE g.id = %s
                GROUP BY g.id, g.episode_id, g.aired, g.canon
                ORDER BY g.episode_id
                """, [game_id],
            )
            row = cur.fetchone()
            if row is None:
                response.status_code = status.HTTP_404_NOT_FOUND
                return {"message": "Game not found"}
            record = {}
            for i, column in enumerate(cur.description):
                record[column.name] = row[i]
            return record


@router.post(
    "/api/custom-games",
    response_model=CustomGame,
)
def create_custom_game():
    with psycopg.connect() as conn:
        with conn.cursor() as cur:
            cur.execute(
                """
                SELECT cats.id, cats.title, cats.canon
                    , clues.id, clues.question, clues.answer
                    , clues.value, clues.invalid_count
                    , clues.canon, clues.category_id
                FROM categories AS cats
                INNER JOIN clues ON (cats.id = clues.category_id)
                WHERE clues.invalid_count = 0 AND cats.canon = true
                ORDER BY RANDOM()
                LIMIT 30;
                """
            )
            clues = cur.fetchall()
            with conn.transaction():
                cur.execute(
                    """
                    INSERT INTO game_definitions (created_on)
                    VALUES (CURRENT_TIMESTAMP);
                    """
                )
                cur.execute(
                    """
                    SELECT gd.id, gd.created_on
                    FROM game_definitions AS gd
                    ORDER BY gd.created_on DESC
                    LIMIT 30;
                    """,
                )
                new_game_definition = cur.fetchone()
                new_game_definition_id = new_game_definition[0]

                for clue in clues:
                    current_clue_id = clue[0]
                    cur.execute(
                        """
                        INSERT INTO game_definition_clues (game_definition_id, clue_id)
                        VALUES (%s, %s)
                        """,
                        [new_game_definition_id, current_clue_id],
                    )
                return {
                    "id": new_game_definition_id,
                    "created_on": new_game_definition[1],
                    "clues":
                    [{
                        "id": clue[3],
                            "question": clue[4],
                            "answer": clue[5],
                            "value": clue[6],
                            "invalid_count": clue[7],
                            "canon": clue[8],
                            "category": {
                                "id": clue[0],
                                "title": clue[1],
                                "canon": clue[2],
                            }
                    } for clue in clues]
                }


@router.get(
    "/api/custom-games/{custom_game_id}",
    response_model=CustomGame
)
def get_custom_game(custom_game_id: int):
    with psycopg.connect() as conn:
        with conn.cursor() as cur:
            cur.execute(
                f"""
                SELECT gd.id, gd.created_on

                    , gdc.game_definition_id, gdc.clue_id

                    , c.id, c.answer, c.question, c.value, c.invalid_count

                    , cats.id, cats.title, cats.canon, c.canon
                FROM game_definitions AS gd
                JOIN game_definition_clues AS gdc
                    ON (gd.id = gdc.game_definition_id)
                JOIN clues AS c
                    ON (gdc.clue_id = c.id)
                JOIN categories AS cats
                    ON (c.category_id = cats.id)
                WHERE gd.id = %s;
                """, [custom_game_id]
            )
            rows = cur.fetchall()
            row = rows[0]
            game_info = {
                "id": row[0],
                "created_on": row[1],
                "clues":
                [{
                    "id": row[4],
                        "answer": row[5],
                        "question": row[6],
                        "value": row[7],
                        "invalid_count": row[8],
                        "canon": row[12],
                        "category": {
                            "id": row[9],
                            "title": row[10],
                            "canon": row[11],
                        }
                    } for row in rows]
            }
            return game_info


@router.get("/api/custom-games")
def get_custom_games():
    with psycopg.connect() as conn:
        with conn.cursor() as cur:
            cur.execute(
                """
                SELECT *
                FROM game_definitions;
                """
            )
            rows = cur.fetchall()
            cur.execute(
                """
                SELECT COUNT(*) FROM game_definitions;
                """
            )
            count = cur.fetchone()[0]
            return({
                "count": count,
                "custom-games": [{
                    "id": row[0],
                    "created_on": row[1]
                } for row in rows]}
            )
