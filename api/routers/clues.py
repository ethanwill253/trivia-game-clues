from unicodedata import category
from fastapi import APIRouter, Response, status
from pydantic import BaseModel
from routers.categories import CategoryOut
from typing import Any
import psycopg

router = APIRouter()


class Clue(BaseModel):
    id: int
    question: str
    answer: str
    value: int
    invalid_count: Any
    canon: bool
    category: CategoryOut

class ClueOut(BaseModel):
    id: int
    answer: str
    question: str

class Clues(BaseModel):
    page_count: int
    clues: list[ClueOut]

class Message(BaseModel):
    message: str


@router.get("/api/clues", response_model=Clues)
def clues_list(page: int = 0):
    with psycopg.connect() as conn:
        with conn.cursor() as cur:
            cur.execute(
                """
                SELECT id, answer, question
                FROM clues
                ORDER by id
                LIMIT 100 OFFSET %s
            """,
                [page * 100],
            )

            results = []
            for row in cur.fetchall():
                record = {}
                for i, column in enumerate(cur.description):
                    record[column.name] = row[i]
                results.append(record)

            cur.execute(
                """
                SELECT COUNT(*) FROM clues;
            """
            )
            raw_count = cur.fetchone()[0]
            page_count = (raw_count // 100) + 1

            return Clues(page_count=page_count, clues=results)


@router.get(
    "/api/clues/{clue_id}",
    response_model=Clue,
    responses={404: {"model": Message},
    500: {"model": Message}}
)
def get_clue(clue_id: int, response: Response):
    with psycopg.connect() as conn:
        with conn.cursor() as cur:
            try:
                cur.execute(
                f"""
                    SELECT cats.id, cats.title, cats.canon,

                        clues.id, clues.question, clues.answer,
                        clues.value, clues.invalid_count, clues.canon
                        , clues.category_id
                    FROM categories AS cats
                    INNER JOIN clues ON (cats.id = clues.category_id)
                    WHERE clues.id = %s
                """,
                    [clue_id],
                )
                row = cur.fetchone()
                if row is None:
                    response.status_code = status.HTTP_404_NOT_FOUND
                    return {"message": "Clue not found"}
                else:
                    return ({
                        "id": row[3],
                        "question": row[4],
                        "answer": row[5],
                        "value": row[6],
                        "invalid_count": row[7],
                        "canon": row[8],
                        "category": {
                            "id": row[0],
                            "title": row[1],
                            "canon": row[2]
                        }
                    })
            except TypeError:
                return


@router.get(
    "/api/random-clue",
    response_model = Clue,
    responses={404: {"model": Message}}
)
def get_random_clue(valid: bool = True):
  with psycopg.connect() as conn:
        with conn.cursor() as cur:
            cur.execute(
               f"""
                SELECT cats.id, cats.title, cats.canon,
                        clues.id, clues.question, clues.answer,
                        clues.value, clues.invalid_count,
                        clues.canon, clues.category_id
                FROM categories AS cats
                INNER JOIN clues ON (cats.id = clues.category_id)
                WHERE clues.invalid_count = 0
                ORDER BY RANDOM()
                LIMIT 1
            """
            )
            row = cur.fetchone()
            return ({
                "id": row[3],
                "question": row[4],
                "answer": row[5],
                "value": row[6],
                "invalid_count": row[7],
                "canon": row[8],
                "category": {
                  "id": row[0],
                  "title": row[1],
                  "canon": row[2],
                }
            })

@router.delete(
  "/api/clues/{clue_id}",
  response_model=Clue,
  responses={400: {"model": Message}},
)
def remove_clue(clue_id: int):
  with psycopg.connect() as conn:
        with conn.cursor() as cur:
          try:
            cur.execute(
                """
                UPDATE clues
                SET invalid_count = invalid_count + 1
                WHERE id = %s;
            """,
                [clue_id],
            )
            cur.execute(
               f"""
                SELECT cats.id, cats.title, cats.canon,
                        clues.id, clues.question, clues.answer,
                        clues.value, clues.invalid_count,
                        clues.canon, clues.category_id
                FROM categories AS cats
                INNER JOIN clues ON (cats.id = clues.category_id)
                WHERE clues.id = %s;
            """,
                [clue_id],
            )
            row = cur.fetchone()
            return {
                "id": row[3],
                "question": row[4],
                "answer": row[5],
                "value": row[6],
                "invalid_count": row[7],
                "canon": row[8],
                "category": {
                  "id": row[0],
                  "title": row[1],
                  "canon": row[2],
                }
            }
          except:
            return {"message": "Unsucessful"}


# class ClueOut(BaseModel):
#   id: int
#   answer: str
#   question: str
#   value: int
#   invalid_count: int
#   category: CategoryOut
#   canon: bool


# @router.get("/api/clues/{clue_id}", response_model=ClueOut)
# def get_clue(clue_id: int, response: Response):
#   with psycopg.connect() as conn:
#     with conn.cursor() as cur:
#       cur.execute(
#           f"""
#           Select clues.id,
#             clues.answer,
#             clues.question,
#             clues.value,
#             clues.invalid_count,
#             categories.id,
#             categories.title,
#             categories.canon,
#             clues.canon
#           From clues
#           Join categories On clues.id = categories.id
#           Where clues.id = %s
#        """,
#           [clue_id]
#       )
#       row = cur.fetchone()
#       if row is None:
#         response.status_code = status.HTTP_404_NOT_FOUND
#         return {"message": "Clue not found"}

#       category = {
#         "id": row[5],
#         "title": row[6],
#         "canon": row[7]
#       }

#       clue = {
#         "id": row[0],
#         "answer": row[1],
#         "question": row[2],
#         "value": row[3],
#         "invalid_count": row[4],
#         "category": category,
#         "canon": row[8]
#       }

#       return clue


# @router.get("/api/random-clue", response_model=ClueOut)
# def get_clue(response: Response):
#   with psycopg.connect() as conn:
#     with conn.cursor() as cur:
#       cur.execute(
#           f"""
#           Select clues.id,
#             clues.answer,
#             clues.question,
#             clues.value,
#             clues.invalid_count,
#             categories.id,
#             categories.title,
#             categories.canon,
#             clues.canon
#           From clues
#           Join categories On clues.id = categories.id
#           Order By RANDOM()
#           Where clues.id = %s
#        """
#       )
#       row = cur.fetchone()
#       if row is None:
#         response.status_code = status.HTTP_404_NOT_FOUND
#         return {"message": "Clue not found"}

#       category = {
#         "id": row[5],
#         "title": row[6],
#         "canon": row[7]
#       }

#       clue = {
#         "id": row[0],
#         "answer": row[1],
#         "question": row[2],
#         "value": row[3],
#         "invalid_count": row[4],
#         "category": category,
#         "canon": row[8]
#       }

#       return clue
